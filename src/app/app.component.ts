import { Component } from '@angular/core';


@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html', 
  styleUrls : ['app.component.css']
})
export class AppComponent {
  title = 'assignment1';
  public number = 0;  
  public isButtonVisible = true;
  
  
  reset(number: any) {
    return this.number = 0

  }
  show(){
    console.log('Show event fired ... !')
    this.isButtonVisible = true
    
    
  }
  hide(){
    console.log('Hide event fired ... !')
    return this.isButtonVisible = false;

    
  }
}
