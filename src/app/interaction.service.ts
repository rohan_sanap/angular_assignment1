import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class InteractionService {

  private _messageSource = new Subject<string>();
  private _messageSource1 = new Subject<string>();
  _messageSource$ = this._messageSource.asObservable();
  _messageSource1$ = this._messageSource1.asObservable();

  constructor() { }

  sendMessageLTR(message : string){
    this._messageSource.next(message);
  }

  sendMessageRTL(message1 : string){
    this._messageSource1.next(message1);
  }
}
