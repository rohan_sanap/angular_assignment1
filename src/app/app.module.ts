import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule, routingComponents } from './app-routing.module';
import { AppComponent } from './app.component';
import { LeftChildComponent } from './left-child/left-child.component';
import { RightChildComponent } from './right-child/right-child.component';
import { GetDataService } from './get-data.service';
import { InteractionService } from './interaction.service';


@NgModule({
  declarations: [
    AppComponent,
    LeftChildComponent,
    RightChildComponent,
    routingComponents
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule
  ],
  providers: [GetDataService, InteractionService],
  bootstrap: [AppComponent]
})
export class AppModule { }
