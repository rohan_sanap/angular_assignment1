import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { catchError, Observable, throwError } from 'rxjs';
import { Idata } from './data';

@Injectable({
  providedIn: 'root'
})
export class GetDataService {

  constructor(private http : HttpClient) { }

  private _myURL = "https://jsonplaceholder.typicode.com/todos"

  getData() : Observable<Idata[]>{
    return this.http.get<Idata[]>(this._myURL).pipe(catchError(this.errorHandling))
  }

  errorHandling (error : HttpErrorResponse){
    return throwError(() => error.name || "Server Error ...!")
  }
}
