import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { InteractionService } from '../interaction.service';

@Component({
  selector: 'app-right-child',
  template: `
    <h2>
      Right Child Component
    </h2>
    <div>
    <h1>{{displayMessage}}</h1>
    </div>
    <button class="float-btn" (click)="decrease(number)" *ngIf="this.isButtonVisible">Decrement</button>
    <div>
    <input class="float-input" #myInput1 type="text"> 
    <button class="float-copy-btn"(click)= "copyRTL(myInput1.value)">COPY</button>
    </div>
  `,
  styleUrls: ['right-child.component.css']
})
export class RightChildComponent implements OnInit {

  @Input('parentData') public number: any;
  @Input('hideData') public isButtonVisible: any;
  public displayMessage = ""


  @Output() public childEvent = new EventEmitter();
  constructor(private _interactionService: InteractionService) { }

  ngOnInit(): void {
    this._interactionService._messageSource$.subscribe(
      message => {
        this.displayMessage = message
      }
    )
  }

  decrease(number: any) {
    console.log("Right button clicked !!")
    this.childEvent.emit(number = number - 1)

  }
  copyRTL( value : any){
    this._interactionService.sendMessageRTL(value)
  }
}
