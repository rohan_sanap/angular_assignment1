import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';
import { ChildActivationEnd } from '@angular/router';
import { InteractionService } from '../interaction.service';

@Component({
  selector: 'app-left-child',
  template: `
  <h2>
  Left Child Component
</h2>
<div>
    <h1>{{displayMessage1}}</h1>
</div>
<button class = "float-btn" (click)="increase(number)" *ngIf="this.isButtonVisible">Increment</button>
<div>
<input class="float-input" #myInput type="text">
<button class="float-copy-btn" (click)= "copy(myInput.value)">COPY</button>
</div>

  `,
  styleUrls: ['left-child.component.css']
})
export class LeftChildComponent implements OnInit {

  public displayMessage1 = ""
  
  @Input('parentData') public number: any;
  @Input('hideData') public isButtonVisible: any;

  @Output() public childEvent = new EventEmitter();

  constructor(private _interactionService: InteractionService) { }

  ngOnInit(): void {
    this._interactionService._messageSource1$.subscribe(
      message1 => {
        this.displayMessage1 = message1
      }
    )
  }

  increase(number: any) {
    console.log(this.number = this.number + 1)
    this.childEvent.emit(number = number + 1)

  }

  copy( value : any){
    this._interactionService.sendMessageLTR(value)
  }

}

