import { Component, OnInit } from '@angular/core';
import { GetDataService } from '../get-data.service';

@Component({
  selector: 'app-get-request',
  templateUrl: './get-request.component.html',
  styleUrls: ['./get-request.component.css']
})
export class GetRequestComponent implements OnInit {

  public datas: {
    userId: number;
    id: number;
    title: string;
    completed: boolean;
  }[] = [];
  public erroMessage = ""
  constructor(private _getDataService: GetDataService) { }

  ngOnInit(): void {
    this._getDataService.getData().subscribe(
      details => this.datas = details,
      error => this.erroMessage = error
    )
  }

  // getData() {
  //   this._getDataService.getData().subscribe(
  //     details => this.datas = details,
  //     error => this.erroMessage = error
  //   )
  // }
}
